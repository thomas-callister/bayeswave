#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <math.h>

#include "BayesLine.h"
#include "BayesWave.h"
#include "BayesWaveIO.h"
#include "BayesWaveMCMC.h"
#include "BayesWaveMath.h"
#include "BayesWavePrior.h"
#include "BayesWaveModel.h"
#include "BayesWaveWavelet.h"
#include "BayesWaveProposal.h"
#include "BayesWaveEvidence.h"
#include "BayesWaveLikelihood.h"

/* ********************************************************************************** */
/*                                                                                    */
/*                                  Checkpointing                                     */
/*                                                                                    */
/* ********************************************************************************** */
#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif

/* This is checked by the main loop to determine when to checkpoint */
static volatile sig_atomic_t __bayeswave_saveStateFlag = 0;

/* This indicates the main loop should terminate */
static volatile sig_atomic_t __bayeswave_exitFlag = 0;

static struct itimerval checkpoint_timer;

/* Signal handler for SIGINT, which is produced by condor
 * when evicting a job, or when the user presses Ctrl-C */
static void catch_interrupt(UNUSED int sig, UNUSED siginfo_t *siginfo,UNUSED void *context)
{

  __bayeswave_saveStateFlag=1;
  __bayeswave_exitFlag=1;
}

/* Signal handler for SIGALRM, for periodic checkpointing */
static void catch_alarm(UNUSED int sig, UNUSED siginfo_t *siginfo,UNUSED void *context)
{
  __bayeswave_saveStateFlag=1;
}


static void restart_sampler(struct Data *data, struct Chain *chain, struct Prior *prior, struct Model **model, char modelname[])
{
  int i,j;
  int ic,ifo;

  int NC = chain->NC;
  int NI = data->NI;

  double Tobs = data->Tobs;

  struct Wavelet *signal  = NULL;
  struct Wavelet **glitch = NULL;

  char filename[128];


  /****************************************************/
  /*                                                  */
  /* PTMCMC TEMPERATURE LADDER                        */
  /*                                                  */
  /****************************************************/
  chain->mc = 0;
  for(ic=0; ic<chain->NC; ic++)
  {
    chain->dT[ic]=chain->tempStep;
    chain->index[ic]=ic;
    if(ic==0)chain->temperature[0] = chain->tempMin;
    else chain->temperature[ic] = chain->temperature[ic-1]*chain->dT[ic-1];
    chain->ptprop[ic]=1;
    chain->ptacc[ic]=0;
    chain->A[ic]=1;
    chain->avgLogLikelihood[ic] = 0.0;
    chain->varLogLikelihood[ic] = 0.0;
  }
  if(chain->NC==1)data->adaptTemperatureFlag=0;

  if(data->adaptTemperatureFlag)// && data->runPhase!=0)
  {
    chain->temperature[chain->NC-1] = 1.0e6;
    //chain->dT[0] = pow(chain->temperature[chain->NC-1],(1./(double)(NC-1)));
    //chain->dT[0] = pow(chain->temperature[chain->NC-1],(1./(double)(NC-1))*(1./(double)(NC-1))*(1./(double)(NC-1)));
    chain->dT[0] = pow(chain->temperature[chain->NC-1],(1./(double)(NC-1))*(1./(double)(NC-1)));
    //chain->dT[0] = pow(chain->temperature[chain->NC-1],(1./(double)(NC-1)));
    for(ic=0; ic<chain->NC-1; ic++)
    {
      //if(chain->NC==15)
      //{
        //chain->temperature[ic]=chain->temperature[ic-1]*chain->dT[0];
        //TODO: DO better initializing chain temperature ladder (hard-wired for NCmax=15!)
        //chain->temperature[ic]=exp((double)(ic*ic)/(double)(((chain->NC-1)*(chain->NC-1))/(log(1.0e6)))); //empirically determined initial guess for temp spacing

        //chain->temperature[ic] = 1./(1.0 - 0.07*(double)ic);
      //}
      //else
      //{
        //chain->temperature[ic] = pow(chain->dT[0],ic*ic*ic);
        chain->temperature[ic] = pow(chain->dT[0],ic*ic);
        //chain->temperature[ic] = pow(chain->dT[0],ic);
      //}
    }

  }


  /****************************************************/
  /*                                                  */
  /* MODEL PARAMETERS                                 */
  /*                                                  */
  /****************************************************/
  for(ic=0; ic<NC; ic++)
  {
    model[ic]->size = 0;

    signal = model[ic]->signal;
    glitch = model[ic]->glitch;

    // clear out whatever is in the signal/glitch model
    for(i=0; i<data->N; i++)
    {
      signal->templates[i] = 0.0;
      for(ifo=0; ifo<NI; ifo++)
      {
        model[ic]->response[ifo][i] = 0.0;
        glitch[ifo]->templates[i] = 0.0;
      }
    }

    // start model parameters anywhere
    if(data->glitchFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        glitch[ifo]->size = data->Dmin;//1;
        model[ic]->size++;
        for(j=1; j<=glitch[ifo]->size; j++)
        {
          draw_wavelet_params(glitch[ifo]->intParams[j], prior->range, &chain->seed, data->NW);
          if(data->amplitudePriorFlag)  data->glitch_amplitude_proposal(glitch[ifo]->intParams[j], model[ic]->Snf[ifo], 1.0, &chain->seed, data->Tobs, prior->range, prior->gSNRpeak);

          model[ic]->wavelet(glitch[ifo]->templates, glitch[ifo]->intParams[j], data->N, 1, Tobs);
        }
      }
    }

    if(data->signalFlag)
    {
      signal->size = 1;
      if(data->rjFlag==0)signal->size=data->Dmin;
      model[ic]->size++;
      //extrinsic_uniform_proposal(&chain->seed,model[ic]->extParams);

      /*
       WaveformProject() was split into two functions:
       -computeProjectionCoeffs() computes the time delays, F+ and Fx, and
       exp(i Phi(f)), which only change with new extrinsic parameters.
       -waveformProject uses the stored coeffs and does the convolution between the
       current geocenter waveform and the detector response function.
       */
      computeProjectionCoeffs(data, model[ic]->projection, model[ic]->extParams, data->fmin, data->fmax);
      Shf_Geocenter_full(data, model[ic]->projection, model[ic]->Snf, model[ic]->SnGeo, model[ic]->extParams);

      for(j=1; j<=signal->size; j++)
      {
        draw_wavelet_params(signal->intParams[j], prior->range, &chain->seed, data->NW);
        if(data->amplitudePriorFlag)  data->signal_amplitude_proposal(signal->intParams[j], model[ic]->SnGeo, 1.0, &chain->seed, data->Tobs, prior->range, prior->sSNRpeak);
        model[ic]->wavelet(signal->templates, signal->intParams[j], data->N, 1, Tobs);
      }
      waveformProject(data, model[ic]->projection, model[ic]->extParams, model[ic]->response, signal->templates, data->fmin, data->fmax);
    }

    // initialize the noise parameters
    for(ifo=0; ifo<NI; ifo++) model[ic]->Sn[ifo] = 1.0;


    // form up residual and compute initial likelihood
    if(data->stochasticFlag)
    {
      //TODO: No support for glitch model in stochastic likelihood
      ComputeNoiseCorrelationMatrix(data, model[ic]->Snf, model[ic]->Sn, model[ic]->background);

      model[ic]->logL = loglike_stochastic(data->NI, data->imin, data->imax, data->r, model[ic]->background->Cij, model[ic]->background->detCij);
    }
    else
    {
      model[ic]->logL = 0.0;
      model[ic]->logLnorm = 0.0;

      for(ifo=0; ifo<NI; ifo++)
      {
        model[ic]->detLogL[ifo] = 0.0;

        for(i=0; i<data->N; i++)
        {
          data->r[ifo][i] = data->s[ifo][i];
          if(data->signalFlag) data->r[ifo][i] -= model[ic]->response[ifo][i];
          if(data->glitchFlag) data->r[ifo][i] -= glitch[ifo]->templates[i];
        }
        if(!data->constantLogLFlag)
        {
          model[ic]->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model[ic]->Sn[ifo], model[ic]->invSnf[ifo]);
          model[ic]->logL += model[ic]->detLogL[ifo];
          for(i=0; i<data->N/2; i++)
          {
            model[ic]->logLnorm -= log(model[ic]->Snf[ifo][i]);
          }

        }
      }
    }

    /****************************************************/
    /*                                                  */
    /* CHAIN FILES                                      */
    /*                                                  */
    /****************************************************/
    if(ic==0 || data->verboseFlag)
    {
      //Basic chain file with likelihood and model dimensions
      sprintf(filename,"chains/%s_model.dat.%i",modelname,ic);
      chain->intChainFile[ic] = fopen(filename,"w");

      //Parameters for reproducing glitch model
      if(data->glitchFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_params_ifo%i.dat.%i",modelname,ifo,ic);
          chain->intGlitchChainFile[ifo][ic] = fopen(filename,"w");
        }
      }

      //Parameters for reproducing signal model
      if(data->signalFlag)
      {
        //intrinsic
        sprintf(filename,"chains/%s_params.dat.%i",modelname,ic);
        chain->intWaveChainFile[ic] = fopen(filename,"w");
      }

      //Parameters for reproducing PSD model
      if(data->bayesLineFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_spline_ifo%i.dat.%i",modelname,ifo,ic);
          chain->splineChainFile[ic][ifo] = fopen(filename,"w");

          sprintf(filename,"chains/%s_lorentz_ifo%i.dat.%i",modelname,ifo,ic);
          chain->lineChainFile[ic][ifo] = fopen(filename,"w");
        }
      }
      
    }//end chain files initialization
  }
}

static void resume_sampler(struct Data *data, struct Chain *chain, struct Model **model, struct BayesLineParams ***bayesline)
{
  //TODO: set internal mcmc flags right based on chain->mc
  int i;
  int ic,ifo;

  int NI = data->NI;
  int NG[NI];
  int NS;
  int NC;

  double logL;

  struct Wavelet *signal  = NULL;
  struct Wavelet **glitch = NULL;

  char filename[128];
  char modelname[128];

  FILE *fptr   = NULL;
  FILE **fptr2 = NULL;

  double **h  = NULL;
  double **g = NULL;

  h = malloc(data->NI*sizeof(double *));
  g = malloc(data->NI*sizeof(double *));
  for(ifo=0; ifo<NI; ifo++)
  {
    g[ifo] = malloc(data->N*sizeof(double));
    h[ifo] = malloc(data->N*sizeof(double));
  }

  /****************************************************/
  /*                                                  */
  /* STATE VECTOR                                     */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/state.dat");
  if( (fptr = fopen(filename,"r")) == NULL )
  {
    fprintf(stderr,"Error:  Could not checkpoint run state\n");
    fprintf(stderr,"        Parameter file %s does not exist\n",filename);
    fprintf(stderr,"        Exiting to system (1)\n");
    exit(1);
  }
  fscanf(fptr,"%s",modelname);
  fscanf(fptr,"%i",&data->cleanModelFlag);
  fscanf(fptr,"%i",&data->noiseModelFlag);
  fscanf(fptr,"%i",&data->glitchModelFlag);
  fscanf(fptr,"%i",&data->signalModelFlag);
  fscanf(fptr,"%i",&data->fullModelFlag);
  fscanf(fptr,"%i",&data->cleanOnlyFlag);
  fscanf(fptr,"%i",&data->loudGlitchFlag);
  fclose(fptr);


  /****************************************************/
  /*                                                  */
  /* DATA AND PSD                                     */
  /*                                                  */
  /****************************************************/
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"checkpoint/data.dat.%i",ifo);
    if( (fptr = fopen(filename,"r")) == NULL )
    {
      fprintf(stderr,"Error:  Could not checkpoint data structure\n");
      fprintf(stderr,"        Parameter file %s does not exist\n",filename);
      fprintf(stderr,"        Exiting to system (1)\n");
      exit(1);
    }
    for(i=0; i<data->N/2; i++)
    {
      fscanf(fptr,"%lg %lg",&data->s[ifo][2*i],&data->s[ifo][2*i+1]);//,&data->Snf[ifo][i],&data->invSnf[ifo][i]);
    }
    fclose(fptr);
  }


  /****************************************************/
  /*                                                  */
  /* PTMCMC TEMPERATURE LADDER                        */
  /*                                                  */
  /****************************************************/
  //Read in dT, index, temperature, ptprop, ptacc, A, avg&var loglikelihood?
  sprintf(filename,"checkpoint/temperature.dat");
  if( (fptr = fopen(filename,"r")) == NULL )
  {
    fprintf(stderr,"Error:  Could not checkpoint temperature model\n");
    fprintf(stderr,"        Parameter file %s does not exist\n",filename);
    fprintf(stderr,"        Exiting to system (1)\n");
    exit(1);
  }
  fscanf(fptr,"%i",&chain->mc);
  fscanf(fptr,"%i",&NC);
  fscanf(fptr,"%i",&chain->zcount);
  fscanf(fptr,"%li",&chain->seed);

  //if(NC != chain->NC) resize_model(data, chain, prior, model, bayesline, model[0]->Snf, NC);

  for(ic=0; ic<chain->NC; ic++)
  {
    fscanf(fptr,"%lg", &chain->dT[ic]);
    fscanf(fptr,"%i",  &chain->index[ic]);
    fscanf(fptr,"%lg", &chain->temperature[ic]);
    fscanf(fptr,"%i",  &chain->ptprop[ic]);
    fscanf(fptr,"%i",  &chain->ptacc[ic]);
    fscanf(fptr,"%lg", &chain->A[ic]);
    fscanf(fptr,"%lg", &chain->avgLogLikelihood[ic]);
    fscanf(fptr,"%lg", &chain->varLogLikelihood[ic]);
  }
  chain->tempMin = chain->temperature[0];
  fclose(fptr);

  sprintf(filename,"checkpoint/logLchain.dat");
  if( (fptr = fopen(filename,"r")) == NULL )
  {
    fprintf(stderr,"Error:  Could not checkpoint likelihood chain\n");
    fprintf(stderr,"        Parameter file %s does not exist\n",filename);
    fprintf(stderr,"        Exiting to system (1)\n");
    exit(1);
  }
  for(i=0; i<chain->zcount; i++)
  {
    for(ic=0; ic<chain->NC; ic++)
    {
      fscanf(fptr,"%lg",&chain->logLchain[ic][i]);
    }
  }
  fclose(fptr);

  /****************************************************/
  /*                                                  */
  /* READ IN STORED MODEL PARAMETERS                  */
  /*                                                  */
  /****************************************************/
  for(ic=0; ic<NC; ic++)
  {
    //Basic chain file with likelihood and model dimensions
    sprintf(filename,"checkpoint/model.dat.%i",ic);
    if( (fptr = fopen(filename,"r")) == NULL )
    {
      fprintf(stderr,"Error:  Could not checkpoint model settings\n");
      fprintf(stderr,"        Parameter file %s does not exist\n",filename);
      fprintf(stderr,"        Exiting to system (1)\n");
      exit(1);
    }

    fscanf(fptr,"%i %lg",&i,&logL); //iteration and logL
    fscanf(fptr,"%i",&NS);                                //signal model dimension
    for(i=0; i<NI; i++)fscanf(fptr,"%i",&NG[i]);          //glitch model dimension
    for(i=0; i<NI; i++)fscanf(fptr,"%lg",&model[ic]->Sn[i]); //PSD scale parameters
    fclose(fptr);

    signal = model[ic]->signal;
    glitch = model[ic]->glitch;

    model[ic]->size = 0;
    signal->size = 0;
    for(ifo=0; ifo<NI; ifo++) glitch[ifo]->size = 0;

    // Glitch model
    if(data->glitchFlag)
    {
      fptr2 = malloc(NI*sizeof(FILE *));
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"checkpoint/params_ifo%i.dat.%i",ifo,ic);
        if( (fptr2[ifo] = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint glitch model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
      }
      parse_glitch_parameters(data, model[ic], fptr2, g);
      for(ifo=0; ifo<NI; ifo++) fclose(fptr2[ifo]);
      free(fptr2);
    }

    // Signal model
    if(data->signalFlag)
    {
      sprintf(filename,"checkpoint/params.dat.%i",ic);
      if( (fptr = fopen(filename,"r")) == NULL)
      {
        fprintf(stderr,"Error:  Could not checkpoint signal model\n");
        fprintf(stderr,"        Parameter file %s does not exist\n",filename);
        fprintf(stderr,"        Exiting to system (1)\n");
        exit(1);
      }
      parse_signal_parameters(data, model[ic], fptr, h, h, h);
      fclose(fptr);
    }

    // PSD model
    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"checkpoint/psd_ifo%i.dat.%i",ifo,ic);
      if( (fptr = fopen(filename,"r")) == NULL)
      {
        fprintf(stderr,"Error:  Could not checkpoint psd model\n");
        fprintf(stderr,"        Parameter file %s does not exist\n",filename);
        fprintf(stderr,"        Exiting to system (1)\n");
        exit(1);
      }
      for(i=0; i<data->N/2; i++)
      {
        fscanf(fptr,"%lg %lg",&model[ic]->Snf[ifo][i],&model[ic]->invSnf[ifo][i]);
      }
      fclose(fptr);

      if(data->bayesLineFlag)
      {
        sprintf(filename,"checkpoint/line_ifo%i.dat.%i",ifo,ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint line model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        parse_line_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/spline_ifo%i.dat.%i",ifo,ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint spline model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        parse_spline_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/bayesline_ifo%i.dat.%i",ifo,ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint bayesline model\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        for(i=0; i< bayesline[ic][ifo]->data->ncut; i++)
        {
          fscanf(fptr,"%lg %lg %lg",&bayesline[ic][ifo]->Snf[i],&bayesline[ic][ifo]->Sbase[i],&bayesline[ic][ifo]->Sline[i]);
        }
        fclose(fptr);

        sprintf(filename,"checkpoint/psd_prior_ifo%i.dat.%i",ifo,ic);
        if( (fptr = fopen(filename,"r")) == NULL)
        {
          fprintf(stderr,"Error:  Could not checkpoint bayesline priors\n");
          fprintf(stderr,"        Parameter file %s does not exist\n",filename);
          fprintf(stderr,"        Exiting to system (1)\n");
          exit(1);
        }
        for(i=0; i<bayesline[ic][ifo]->data->ncut; i++)
        {
          fscanf(fptr,"%lg %lg %lg %lg",&bayesline[ic][ifo]->priors->sigma[i],&bayesline[ic][ifo]->priors->mean[i],&bayesline[ic][ifo]->priors->lower[i],&bayesline[ic][ifo]->priors->upper[i]);
        }
        fclose(fptr);
      }//end bayeslineFlag
    }

    // Check that the model dimensions match
    for(ifo=0; ifo<NI; ifo++)
    {
      model[ic]->size += glitch[ifo]->size;
      if(glitch[ifo]->size != NG[ifo])
      {
        fprintf(stderr,"Error:  Could not checkpoint glitch model\n");
        fprintf(stderr,"        Model dimensions do not match (IFO%i:  %i!=%i)\n",ifo,glitch[ifo]->size,NG[ifo]);
        fprintf(stderr,"        Exiting to system (1)\n");
        exit(1);
      }
    }
    model[ic]->size += signal->size;
    if(signal->size != NS)
    {
      fprintf(stderr,"Error:  Could not checkpoint signal model\n");
      fprintf(stderr,"        Model dimensions do not match (%i!=%i)\n",signal->size,NS);
      fprintf(stderr,"        Exiting to system (1)\n");
      exit(1);
    }


    // form up residual and compute initial likelihood
    model[ic]->logL = 0.0;
    model[ic]->logLnorm = 0.0;

    for(ifo=0; ifo<NI; ifo++)
    {
      model[ic]->detLogL[ifo] = 0.0;

      for(i=0; i<data->N; i++)
      {
        data->r[ifo][i] = data->s[ifo][i];
        if(data->signalFlag) data->r[ifo][i] -= model[ic]->response[ifo][i];
        if(data->glitchFlag) data->r[ifo][i] -= glitch[ifo]->templates[i];
      }
      if(!data->constantLogLFlag)
      {
        model[ic]->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model[ic]->Sn[ifo], model[ic]->invSnf[ifo]);
        model[ic]->logL += model[ic]->detLogL[ifo];
        for(i=0; i<data->N/2; i++)
        {
          model[ic]->logLnorm -= log(model[ic]->Snf[ifo][i]);
        }

      }
    }
    
    /****************************************************/
    /*                                                  */
    /* REOPEN CHAIN FILES                               */
    /*                                                  */
    /****************************************************/
    if(ic==0 || data->verboseFlag)
    {
      //Basic chain file with likelihood and model dimensions
      sprintf(filename,"chains/%s_model.dat.%i",modelname,ic);
      chain->intChainFile[ic] = fopen(filename,"a");

      //Parameters for reproducing glitch model
      if(data->glitchFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_params_ifo%i.dat.%i",modelname,ifo,ic);
          chain->intGlitchChainFile[ifo][ic] = fopen(filename,"a");
        }
      }

      //Parameters for reproducing signal model
      if(data->signalFlag)
      {
        //intrinsic
        sprintf(filename,"chains/%s_params.dat.%i",modelname,ic);
        chain->intWaveChainFile[ic] = fopen(filename,"a");
      }

      //Parameters for reproducing PSD model
      if(data->bayesLineFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          sprintf(filename,"chains/%s_spline_ifo%i.dat.%i",modelname,ifo,ic);
          chain->splineChainFile[ic][ifo] = fopen(filename,"a");

          sprintf(filename,"chains/%s_lorentz_ifo%i.dat.%i",modelname,ifo,ic);
          chain->lineChainFile[ic][ifo] = fopen(filename,"a");
        }
      }
      
    }//end chain files initialization
  }

  /****************************************************/
  /*                                                  */
  /* CLEAR OUT CHECKPOINT DIRECTORY                   */
  /*                                                  */
  /****************************************************/
  fprintf(stdout,"...resuming model %s at iteration %i\n",modelname,chain->mc);
  fflush(stdout);

  for(ifo=0; ifo<NI; ifo++)
  {
    free(g[ifo]);
    free(h[ifo]);
  }
  free(g);
  free(h);
}

static void save_sampler(struct Data *data, struct Chain *chain, struct Model **model, struct BayesLineParams ***bayesline, char modelname[])
{
  int i;
  int ic,ifo;

  int NI = data->NI;

  struct Wavelet *signal  = NULL;
  struct Wavelet **glitch = NULL;

  char filename[128];

  FILE *fptr = NULL;

  /****************************************************/
  /*                                                  */
  /* STATE VECTOR                                     */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/state.dat");
  fptr = fopen(filename,"w");
  fprintf(fptr,"%s ",modelname);
  fprintf(fptr,"%i ",data->cleanModelFlag);
  fprintf(fptr,"%i ",data->noiseModelFlag);
  fprintf(fptr,"%i ",data->glitchModelFlag);
  fprintf(fptr,"%i ",data->signalModelFlag);
  fprintf(fptr,"%i ",data->fullModelFlag);
  fprintf(fptr,"%i ",data->cleanOnlyFlag);
  fprintf(fptr,"%i ",data->loudGlitchFlag);
  fprintf(fptr,"\n");
  fclose(fptr);

  /****************************************************/
  /*                                                  */
  /* DATA AND PSD                                     */
  /*                                                  */
  /****************************************************/
  for(ifo=0; ifo<NI; ifo++)
  {
    sprintf(filename,"checkpoint/data.dat.%i",ifo);
    fptr = fopen(filename,"w");
    for(i=0; i<data->N/2; i++)
    {
      fprintf(fptr,"%.12g %.12g\n",data->s[ifo][2*i],data->s[ifo][2*i+1]);
    }
    fclose(fptr);
  }

  /****************************************************/
  /*                                                  */
  /* PTMCMC TEMPERATURE LADDER                        */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/temperature.dat");
  fptr = fopen(filename,"w");

  fprintf(fptr,"%i\n",chain->mc);
  fprintf(fptr,"%i\n",chain->NC);
  fprintf(fptr,"%i\n",chain->zcount);
  fprintf(fptr,"%li\n",chain->seed);

  for(ic=0; ic<chain->NC; ic++)
  {
    fprintf(fptr,"%lg ", chain->dT[ic]);
    fprintf(fptr,"%i ",  chain->index[ic]);
    fprintf(fptr,"%lg ", chain->temperature[ic]);
    fprintf(fptr,"%i ",  chain->ptprop[ic]);
    fprintf(fptr,"%i ",  chain->ptacc[ic]);
    fprintf(fptr,"%lg " , chain->A[ic]);
    fprintf(fptr,"%lg ", chain->avgLogLikelihood[ic]);
    fprintf(fptr,"%lg ", chain->varLogLikelihood[ic]);
    fprintf(fptr,"\n");
  }
  fclose(fptr);

  sprintf(filename,"checkpoint/logLchain.dat");
  fptr = fopen(filename,"w");
  for(i=0; i<chain->zcount; i++)
  {
    for(ic=0; ic<chain->NC; ic++)
    {
      fprintf(fptr,"%.12g ",chain->logLchain[ic][i]);
    }
    fprintf(fptr,"\n");
  }
  fclose(fptr);

  /****************************************************/
  /*                                                  */
  /* WRITE OUT CURRENT MODEL PARAMETERS               */
  /*                                                  */
  /****************************************************/
  for(ic=0; ic<chain->NC; ic++)
  {
    signal = model[ic]->signal;
    glitch = model[ic]->glitch;

    //Basic chain file with likelihood and model dimensions
    sprintf(filename,"checkpoint/model.dat.%i",ic);
    fptr = fopen(filename,"w");
    fprintf(fptr,"%i %.12g ",i,model[ic]->logL);                       //iteration and logL
    fprintf(fptr,"%i ",signal->size);                                //signal model dimension
    for(i=0; i<NI; i++)fprintf(fptr,"%i ",glitch[i]->size);          //glitch model dimension
    for(i=0; i<NI; i++)fprintf(fptr,"%lg ",model[ic]->Sn[i]); //PSD scale parameters
    fprintf(fptr,"\n");
    fclose(fptr);

    // Glitch model
    if(data->glitchFlag)
    {
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"checkpoint/params_ifo%i.dat.%i",ifo,ic);
        fptr = fopen(filename,"w");
        print_glitch_model(fptr, glitch[ifo],data->NW);
        fclose(fptr);
      }
    }

    // Signal model
    if(data->signalFlag)
    {
      sprintf(filename,"checkpoint/params.dat.%i",ic);
      fptr = fopen(filename,"w");
      print_signal_model(fptr, model[ic]);
      fclose(fptr);
    }

    // PSD model
    for(ifo=0; ifo<NI; ifo++)
    {
      sprintf(filename,"checkpoint/psd_ifo%i.dat.%i",ifo,ic);
      fptr = fopen(filename,"w");
      for(i=0; i<data->N/2; i++)
      {
        fprintf(fptr,"%lg %lg\n",model[ic]->Snf[ifo][i],model[ic]->invSnf[ifo][i]);
      }
      fclose(fptr);

      if(data->bayesLineFlag)
      {
        sprintf(filename,"checkpoint/line_ifo%i.dat.%i",ifo,ic);
        fptr = fopen(filename,"w");
        print_line_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/spline_ifo%i.dat.%i",ifo,ic);
        fptr = fopen(filename,"w");
        print_spline_model(fptr, bayesline[ic][ifo]);
        fclose(fptr);

        sprintf(filename,"checkpoint/bayesline_ifo%i.dat.%i",ifo,ic);
        fptr = fopen(filename,"w");
        for(i=0; i< bayesline[ic][ifo]->data->ncut; i++)
        {
          fprintf(fptr,"%lg %lg %lg\n",bayesline[ic][ifo]->Snf[i],bayesline[ic][ifo]->Sbase[i],bayesline[ic][ifo]->Sline[i]);
        }
        fclose(fptr);

        sprintf(filename,"checkpoint/psd_prior_ifo%i.dat.%i",ifo,ic);
        fptr = fopen(filename,"w");
        for(i=0; i<bayesline[ic][ifo]->data->ncut; i++)
        {
          fprintf(fptr,"%lg %lg %lg %lg\n",bayesline[ic][ifo]->priors->sigma[i],bayesline[ic][ifo]->priors->mean[i],bayesline[ic][ifo]->priors->lower[i],bayesline[ic][ifo]->priors->upper[i]);
        }
        fclose(fptr);
      }//end bayeslineFlag
    }//loop over IFOs
  }//loop over chains
  
  /****************************************************/
  /*                                                  */
  /* WRITE OUT CURRENT EVIDENCE RESULTS               */
  /*                                                  */
  /****************************************************/
  sprintf(filename,"checkpoint/evidence.dat");
  fptr = fopen(filename,"w");
  fprintf(fptr,"%.12g %lg\n",data->logZnoise,data->varZnoise);
  fprintf(fptr,"%.12g %lg\n",data->logZglitch,data->varZglitch);
  fprintf(fptr,"%.12g %lg\n",data->logZsignal,data->varZsignal);
  fprintf(fptr,"%.12g %lg\n",data->logZfull,data->varZfull);
  fclose(fptr);

  fprintf(stdout,"...saved model %s at iteration %i\n",modelname,chain->mc);
}

/* ********************************************************************************** */
/*                                                                                    */
/*                                  MCMC Samplers                                     */
/*                                                                                    */
/* ********************************************************************************** */

void RJMCMC(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct Chain *chain, struct Prior *prior, double *logEvidence, double *varLogEvidence)
{

  print_run_stats(stdout, data, chain);
  print_run_flags(stdout, data, prior);

  int N  = data->N;
  int NI = data->NI;
  int M  = chain->count;
  int NC = chain->NC;

  int i, ic, ifo;

  double logZ = 0.0;
  double varZ = 0.0;

  double logPost;
  double logPostMap=-1e60;

  char filename[100];
  char modelname[100];

  // parameters to control how many frames are output to model animations
  int frame=0;
  int frameCount=200;

  // gnuplot animations cadence can be linear or logarithmic in time
  //TODO: linear/log frame cadence should be a command line switch
  //double frameInterval = log((double)M)/(double)frameCount;
  double frameInterval = (double)M/(double)frameCount;

  // parameters for autocorrelation length
  chain->zcount=0;

  // Store MAP model
  struct Model *model_MAP = malloc(sizeof(struct Model));
  initialize_model(model_MAP, data, prior, model[0]->Snf, &chain->seed);


  /******************************************************************************/
  /*                                                                            */
  /*  Set up checkpointing                                                      */
  /*    Code taken from LALInferenceNestedSamplingAlgorithm.c                   */
  /*    courtesy of John Veitch                                                 */
  /*                                                                            */
  /******************************************************************************/
  if(data->checkpointFlag)
  {
    /* Install a periodic alarm that will trigger a checkpoint */
    int sigretcode=0;
    struct sigaction sa;
    sa.sa_sigaction=catch_alarm;
    sa.sa_flags=SA_SIGINFO;
    sigretcode=sigaction(SIGVTALRM,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint timer!\n");

    /* Condor sends SIGUSR2 to checkpoint and continue */
    sigretcode=sigaction(SIGUSR2,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint on SIGUSR2.\n");
    checkpoint_timer.it_interval.tv_sec=3600; /* Default timer 1 hour */
    checkpoint_timer.it_interval.tv_usec=0;
    checkpoint_timer.it_value=checkpoint_timer.it_interval;
    setitimer(ITIMER_VIRTUAL,&checkpoint_timer,NULL);

    /* Install the handler for the condor interrupt signal */
    sa.sa_sigaction=catch_interrupt;
    sigretcode=sigaction(SIGINT,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint on SIGINT.\n");

    /* Condor sends SIGTERM to vanilla universe jobs to evict them */
    sigretcode=sigaction(SIGTERM,&sa,NULL);
    if(sigretcode!=0) fprintf(stderr,"WARNING: Cannot establish checkpoint on SIGTERM.\n");
    /* Condor sends SIGTSTP to standard universe jobs to evict them.
     *I think condor handles this, so didn't add a handler CHECK */
  }
  /******************************************************************************/
  /*                                                                            */
  /*  Set up data structures                                                    */
  /*                                                                            */
  /******************************************************************************/

  /*
   OUTPUT DATA
   */

  //give output files common name
  if(data->runPhase==0)sprintf(modelname,"%sclean",data->runName);
  else
  {
    if(!data->glitchFlag && !data->signalFlag)
      sprintf(modelname,"%snoise",data->runName);

    else if(data->glitchFlag && !data->signalFlag)
      sprintf(modelname,"%sglitch",data->runName);

    else if(!data->glitchFlag && data->signalFlag)
      sprintf(modelname,"%ssignal",data->runName);

    else
      sprintf(modelname,"%sfull",data->runName);
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Print run stats to file in current directory                              */
  /*  so that the run is replicable                                             */
  /*                                                                            */
  /******************************************************************************/

  sprintf(filename,"%s.run",modelname);
  chain->runFile = fopen(filename,"w");

  fprintf(chain->runFile,"\n =========== BayesWaveBurst ===========\n");
  fprintf(chain->runFile,"  %s\n\n",LALInferencePrintCommandLine(data->commandLine) );
  print_run_flags(chain->runFile, data, prior);

  FILE *tfile = NULL;
  if(data->verboseFlag)
  {
    //Basic chain file with likelihood and model dimensions
    sprintf(filename,"chains/%s_temperature.dat",modelname);
    tfile = fopen(filename,"w");
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Set up each model's parameters and output files                           */
  /*                                                                            */
  /******************************************************************************/

  chain->intPropRate = 0.5; //split intrinsic proposals between U[prior] and Fisher

  int burnFlag   = 0;
  int searchFlag = 0;

  print_run_stats(chain->runFile, data, chain);

  if(data->checkpointFlag && data->resumeFlag)
  {
    printf("try resuming...\n");
    resume_sampler(data, chain, model, bayesline);
    data->resumeFlag = 0;

    /******************************************************************************/
    /*                                                                            */
    /*  Make sure run settings are correct for current sample                     */
    /*                                                                            */
    /******************************************************************************/

    if(chain->mc>chain->burn) burnFlag=1;
  }
  else restart_sampler(data, chain, prior, model, modelname);


  //Set MAP model to initial state
  copy_psd_model(model[chain->index[0]], model_MAP, N, NI);
  copy_ext_model(model[chain->index[0]], model_MAP, N, NI);
  copy_int_model(model[chain->index[0]], model_MAP, N, NI, data->NW, -1);
  for(ifo=0; ifo<NI; ifo++) copy_int_model(model[chain->index[0]], model_MAP, N, NI,data->NW, ifo);

  FILE *script = NULL;
  FILE *psdscript = NULL;
  FILE *bwscript = NULL;
  FILE *logLscript = NULL;

  if(data->gnuplotFlag)
  {

    /******************************************************************************/
    /*                                                                            */
    /*  Set up gnuplot script for waveform movies                                 */
    /*                                                                            */
    /******************************************************************************/
    //write_gnuplot_script_header(script, modelname,data->Tobs);
    sprintf(filename,"waveforms/%s_waveforms_animate.gpi",modelname);
    script=fopen(filename,"w");
    fprintf(script,"set terminal gif small size 800,600 animate optimize delay 0.1 enhanced font\n");
    fprintf(script,"set output \"%s_waveforms.gif\"                     \n",modelname);
    fprintf(script,"                                   \n");
    //fprintf(script,"set xrange[%g:%g]                  \n", data->Tobs-2.0-0.06, data->Tobs-2.0+0.06);
    fprintf(script,"set xrange[%g:%g]                  \n", -data->Twin/2, +data->Twin/2);
    //fprintf(script,"set yrange[-40:40]                 \n");
    //fprintf(script,"set xtics 0.02                     \n");
    fprintf(script,"                                   \n");
    fprintf(script,"set xlabel \"time (s)\"            \n");
    fprintf(script,"set ylabel \"whitened strain\"     \n");
    fprintf(script,"                                   \n");
    fflush(script);

    /******************************************************************************/
    /*                                                                            */
    /*  Set up gnuplot script for PSD movies                                      */
    /*                                                                            */
    /******************************************************************************/
    if(data->bayesLineFlag)
    {
      sprintf(filename,"waveforms/%s_psd_animate.gpi",modelname);
      psdscript=fopen(filename,"w");
      fprintf(psdscript,"set terminal gif small size 800,600 animate optimize delay 0.1 enhanced font\n");
      fprintf(psdscript,"set output \"%s_psd.gif\"          \n",modelname);
      fprintf(psdscript,"                                   \n");
      fprintf(psdscript,"set xrange[%g:%g]                  \n", data->fmin, data->fmax);
      fprintf(psdscript,"set yrange[1e-48:1e-37]            \n");
      fprintf(psdscript,"set logscale y                     \n");
      fprintf(psdscript,"                                   \n");
      fprintf(psdscript,"set xlabel \"frequency (Hz)\"      \n");
      fprintf(psdscript,"set ylabel \"PSD\"                 \n");
      fprintf(psdscript,"                                   \n");
      fflush(psdscript);
    }

    /******************************************************************************/
    /*                                                                            */
    /*  Set up gnuplot script for BayesWave movies                                */
    /*                                                                            */
    /******************************************************************************/
    if(data->bayesLineFlag)
    {
      sprintf(filename,"waveforms/%s_bayeswave_animate.gpi",modelname);
      bwscript=fopen(filename,"w");
      fprintf(bwscript,"set terminal gif small size 800,600 animate optimize delay 0.1 enhanced font\n");
      fprintf(bwscript,"set output \"%s_bayeswave.gif\"          \n",modelname);
      fprintf(bwscript,"                                         \n");
      fflush(bwscript);
    }

    /******************************************************************************/
    /*                                                                            */
    /*  Set up gnuplot script for <logL> movies                                   */
    /*                                                                            */
    /******************************************************************************/
    sprintf(filename,"waveforms/%s_avgLogL_animate.gpi",modelname);
    logLscript=fopen(filename,"w");
    fprintf(logLscript,"set terminal gif small size 800,600 animate optimize delay 0.5 enhanced font\n");
    fprintf(logLscript,"set output \"%s_avgLogL.gif\"          \n",modelname);
    fprintf(logLscript,"                                       \n");
    fprintf(logLscript,"set logscale x                         \n");
    fprintf(logLscript,"set xrange [1e-8:1]                    \n");
    fprintf(logLscript,"set xlabel \"1/T\"                     \n");
    fprintf(logLscript,"set ylabel \"<logL>\"                  \n");
    fflush(logLscript);

  }

  /******************************************************************************/
  /*                                                                            */
  /*  Set up time-frequency proposal for wavelets                               */
  /*                                                                            */
  /******************************************************************************/
  struct TimeFrequencyMap *tf = malloc(sizeof(struct TimeFrequencyMap));

  if(data->signalFlag || data->glitchFlag)
  {
    initialize_TF_proposal(data, prior, tf);

    //set up proposal for glitch model
    for(ifo=0; ifo<NI; ifo++)
    {
      printf("Setting up time-frequency proposal for IFO%i...\n",ifo);
      TFprop_setup(data, model[chain->index[0]], prior->range, tf, ifo);
      if(data->gnuplotFlag) TFprop_plot(prior->range, tf, (prior->range[0][1]-prior->range[0][0])/(double)(tf->nt), ifo);
    }
      if(data->signalFlag)
      {
          printf("Setting up time-frequency proposal for signal model...\n");
          TFprop_signal(data, prior->range, tf, model[chain->index[0]]->projection);
      }
  }



  /******************************************************************************/
  /*                                                                            */
  /*  The BurstRJMCMC loop                                                      */
  /*                                                                            */
  /******************************************************************************/

  while(chain->mc<M)
  {
    //Checkpoint the start of the chain for insurance purposes
    if(data->checkpointFlag && chain->mc==0) save_sampler(data, chain, model, bayesline, modelname);

    //Reset counters after initial burn-in & switch to Markovian likelihood function
    if(burnFlag==0 && chain->mc>chain->burn)  burnFlag=1;

    //print state of cold chain to file
    print_chain_files(data, chain, model, bayesline, 0);
    //if(chain->mc%1000) flush_chain_files(data,chain,0);
    if(data->verboseFlag)
    {
      //only print hot chains if asked to
      for(ic=1; ic<NC; ic++)print_chain_files(data, chain, model, bayesline, ic);
    }


    //print status update to screen
    if(chain->mc%(M/10)==0 || data->verboseFlag)
    {
      print_chain_status(data, chain, model, searchFlag);
    }

    //Loop over chains doing model updates
    for(ic=0; ic<NC; ic++)
    {
      chain->beta = 1./chain->temperature[ic];

      //This function executes [cycle] intrinsic parameter updates for the geocenter signal(RJMCMC or MCMC)
      if(!data->fixIntrinsicFlag) EvolveIntrinsicParameters(data, prior, model, chain, tf, &chain->seed, ic);

      //This function executes [cycle] extrinsic parameter updates, common to all geocenter wavelets.
      if(model[chain->index[ic]]->signal->size>0 && !data->fixExtrinsicFlag) EvolveExtrinsicParameters(data, prior, model, chain, &chain->seed, ic);

      //update PSD with BayesLine
      if(data->bayesLineFlag) EvolveBayesLineParameters(data, model, bayesline, chain, prior, ic);
    }

    //After so many iterations recompute the residuals and likelihood (prevent accumulation of roundoff error)
    recompute_residual(data, model, chain);
    if(burnFlag==0 && chain->mc%1000==0 && data->signalFlag) TFprop_signal(data, prior->range, tf, model[chain->index[0]]->projection);


    //During cleaning phase track max and min BL PSDs to set up priors for model selection run
    if(data->bayesLineFlag && data->cleanFlag )
    {
      ic = chain->index[0];
      for(ifo=0; ifo<NI; ifo++)
      {
        for(i=0; i< bayesline[ic][ifo]->data->ncut; i++)
        {
          if(bayesline[ic][ifo]->Snf[i] > bayesline[ic][ifo]->priors->upper[i])
            bayesline[ic][ifo]->priors->upper[i] = bayesline[ic][ifo]->Snf[i]*10.;
          else if(bayesline[ic][ifo]->Snf[i] < bayesline[ic][ifo]->priors->lower[i])
            bayesline[ic][ifo]->priors->lower[i] = bayesline[ic][ifo]->Snf[i]/10.;
        }
      }
    }
    
    //Parallel tempering
    if(NC>1 && chain->mc>1)
    {
      PTMCMC(model, chain, &chain->seed, NC); /*PTMCMC moved inside of EvolveParameters*/

      if(data->verboseFlag)
      {
        for(ic=0; ic<NC; ic++) fprintf(tfile,"%g ",chain->temperature[ic]);
        for(ic=0; ic<NC; ic++) fprintf(tfile,"%g ",chain->A[ic]);
        for(ic=0; ic<NC; ic++) fprintf(tfile,"%g ",(double)chain->ptacc[ic]/(double)chain->ptprop[ic]);
        fprintf(tfile,"\n");
	fflush(tfile);
      }

      //adapt PTMCMC spacing and reset various counters
      if(chain->mc<M/2 && data->adaptTemperatureFlag)
      {
          adapt_temperature_ladder(chain, NC);
          if(chain->mc%100000==0)
          {
              for(ic=0; ic<NC; ic++)
              {
                  chain->ptprop[ic] = 1;
                  chain->ptacc[ic]  = 1;
              }
          }
      }

    }//end NC>1 condition for parallel tempering


    // take an occasional look at the whitened reconstructed waveforms in the time domain (print ~100 total)
    if(chain->mc>=frame*frameInterval && data->gnuplotFlag)
    {
      //print waveforms to file for diagnostics
      for(ifo=0; ifo<NI; ifo++)
      {
        sprintf(filename,"waveforms/%s_data_%d.dat.%i", modelname, frame,ifo);
        print_time_domain_waveforms(filename, data->s[ifo], N, model[chain->index[0]]->Snf[ifo], model[chain->index[0]]->Sn[ifo],data->Tobs, data->imin, data->imax, prior->range[0][0], prior->range[0][1]);

        if(data->bayesLineFlag)
        {
          for(i=0; i<N; i++) data->r[ifo][i] = data->s[ifo][i] - model[chain->index[0]]->glitch[ifo]->templates[i];
          sprintf(filename,"waveforms/%s_frequency_residual_%d.dat.%i",modelname,frame,ifo);
          print_frequency_domain_waveforms(filename, data->r[ifo], N, model[chain->index[0]]->Snf[ifo], data->Tobs, model[chain->index[0]]->Sn[ifo], data->imin, data->imax);
        }

        if(data->glitchFlag)
        {
          sprintf(filename,"waveforms/%s_glitch_%d.dat.%i", modelname, frame,ifo);
          print_time_domain_waveforms(filename, model[chain->index[0]]->glitch[ifo]->templates, N, model[chain->index[0]]->Snf[ifo],model[chain->index[0]]->Sn[ifo],data->Tobs, data->imin, data->imax, prior->range[0][0], prior->range[0][1]);

          sprintf(filename,"waveforms/%s_colored_glitch_%d.dat.%i", modelname, frame,ifo);
          print_colored_time_domain_waveforms(filename, model[chain->index[0]]->glitch[ifo]->templates, N, model[chain->index[0]]->Snf[ifo],model[chain->index[0]]->Sn[ifo],data->Tobs, data->imin, data->imax, prior->range[0][0], prior->range[0][1]);
        }

        if(data->signalFlag)
        {
          sprintf(filename,"waveforms/%s_wave_%d.dat.%i", modelname, frame, ifo);
          print_time_domain_waveforms(filename, model[chain->index[0]]->response[ifo], N, model[chain->index[0]]->Snf[ifo],model[chain->index[0]]->Sn[ifo],data->Tobs, data->imin, data->imax, prior->range[0][0], prior->range[0][1]);
          
          sprintf(filename,"waveforms/%s_colored_wave_%d.dat.%i", modelname, frame, ifo);
          print_colored_time_domain_waveforms(filename, model[chain->index[0]]->response[ifo], N, model[chain->index[0]]->Snf[ifo],model[chain->index[0]]->Sn[ifo],data->Tobs, data->imin, data->imax, prior->range[0][0], prior->range[0][1]);
        }

        if(data->bayesLineFlag)
        {
          sprintf(filename,"waveforms/%s_psd_%d.dat.%i", modelname, frame,ifo);
          print_bayesline_spectrum(filename, bayesline[chain->index[0]][ifo]->freq, bayesline[chain->index[0]][ifo]->spow, bayesline[chain->index[0]][ifo]->Sbase, bayesline[chain->index[0]][ifo]->Sline, bayesline[chain->index[0]][ifo]->data->ncut);
          sprintf(filename,"waveforms/%s_psd_%d.dat.%i.hot", modelname, frame,ifo);
          print_bayesline_spectrum(filename, bayesline[chain->index[chain->NC-1]][ifo]->freq, bayesline[chain->index[chain->NC-1]][ifo]->spow, bayesline[chain->index[chain->NC-1]][ifo]->Sbase, bayesline[chain->index[chain->NC-1]][ifo]->Sline, bayesline[chain->index[chain->NC-1]][ifo]->data->ncut);
        }
      }
      write_gnuplot_script_frame(script, modelname, data->signalFlag, data->glitchFlag, frame, data->NI);
      if(data->bayesLineFlag)
      {
        write_bayesline_gnuplot_script_frame(psdscript, modelname, frame);
        write_bayeswave_gnuplot_script_frame(bwscript, modelname, data->Tobs, data->fmin, data->fmax, data->runPhase, data->signalFlag, data->glitchFlag, frame, data->NI);
      }
      frame++;
    }


    //Update MAP model
    if(chain->mc > M/2)
    {
      logPost = model[chain->index[0]]->logL;
      if(data->glitchFlag) for(ifo=0; ifo<NI; ifo++) logPost += model[chain->index[0]]->glitch[ifo]->logp;
      if(data->signalFlag)                           logPost += model[chain->index[0]]->signal->logp;
      if(logPost > logPostMap)
      {
        logPostMap = logPost;
        copy_psd_model(model[chain->index[0]], model_MAP, N, NI);
        copy_ext_model(model[chain->index[0]], model_MAP, N, NI);
        copy_int_model(model[chain->index[0]], model_MAP, N, NI, data->NW, -1);
        for(ifo=0; ifo<NI; ifo++) copy_int_model(model[chain->index[0]], model_MAP, N, NI, data->NW, ifo);
//        LaplaceApproximation(data, model_MAP, chain, prior, &logZ);
      }
    }

    // Compute Evidence/Bayes Factor using last 1/4 of samples
    if(chain->mc >= M/4)
    {
      for(ic=0; ic<NC; ic++) chain->logLchain[ic][chain->zcount] = model[chain->index[ic]]->logL + model[chain->index[ic]]->logLnorm - data->logLc;
      chain->zcount++;

//      if(chain->mc%(M/1)==0 && !data->constantLogLFlag)TrapezoidIntegration(chain, chain->zcount, modelname, &logZ, &varZ);
    }

    chain->mc+=chain->cycle;

    // Save progress
    if(__bayeswave_saveStateFlag)
    {
      fprintf(stdout,"Interrupt/Timer message recieved\n");
      fprintf(stdout,"Saving state to checkpoint directory\n");//,resumefilename);
      save_sampler(data, chain, model, bayesline, modelname);
      __bayeswave_saveStateFlag = 0;
      fflush(stdout);
    }
    // Exit gracefully if interrupted
    if(__bayeswave_exitFlag)
    {
      fprintf(stdout,"Exiting to system with error code 130\n");
      fprintf(stdout,"\n");
      fflush(stdout);
      exit(130);
    }

  }


  /******************************************************************************/
  /*                                                                            */
  /*  Here we are experimenting with different ways to                          */
  /*  estimate the mean and variance of each chain                              */
  /*                                                                            */
  /******************************************************************************/
  if(!data->constantLogLFlag)
  {

    /******************************************************************************/
    /*                                                                            */
    /*  Laplace approximation to evidence                                         */
    /*                                                                            */
    /******************************************************************************/
//    LaplaceApproximation(data, model_MAP, chain, prior, &logZ);
//
//    printf("Laplace Approximation logZ = %g\n",logZ);

    /******************************************************************************/
    /*                                                                            */
    /*  Thermodynamic Integration via Trapezoid Rule                              */
    /*                                                                            */
    /******************************************************************************/
    TrapezoidIntegration(chain, chain->nPoints, modelname, &logZ, &varZ);

    printf("Trapezoid Rule logZ = %g +/- %g\n",logZ,sqrt(varZ));

    /******************************************************************************/
    /*                                                                            */
    /*  Monte Carlo integration of <logL> v beta                                  */
    /*  including chain-to-chain correlations and monotonic prior                 */
    /*                                                                            */
    /******************************************************************************/
    if(data->splineEvidenceFlag)
    {
      ThermodynamicIntegration(chain->temperature, chain->logLchain, chain->nPoints, chain->NC, modelname, &logZ, &varZ);

      printf("Thermodynamic Integration logZ = %g +/- %g\n",logZ,sqrt(varZ));
    }
  }

  /******************************************************************************/
  /*                                                                            */
  /*  Return logZ, and exit cleanly so we can test other models                 */
  /*                                                                            */
  /******************************************************************************/

  free_model(model_MAP, data, prior);
  free(model_MAP);

  fclose(chain->runFile);

  if(data->gnuplotFlag)
  {
    fclose(script);
    if(data->bayesLineFlag)
    {
      fclose(bwscript);
      fclose(psdscript);
    }
  }

  for(ic=0; ic<NC; ic++)
  {
    if(ic==0 || data->verboseFlag)
    {
      if(data->glitchFlag || data->signalFlag)
      {
        fclose(chain->intChainFile[ic]);
        if(data->signalFlag)
          fclose(chain->intWaveChainFile[ic]);
        if(data->glitchFlag) for(ifo=0; ifo<NI; ifo++)
          fclose(chain->intGlitchChainFile[ifo][ic]);
      }

      if(data->bayesLineFlag)
      {
        for(ifo=0; ifo<NI; ifo++)
        {
          fclose(chain->splineChainFile[ic][ifo]);
          fclose(chain->lineChainFile[ic][ifo]);

        }
      }

    }
  }

  if(data->signalFlag || data->glitchFlag)
  {
    free_TF_proposal(data, tf);
  }
  free(tf);

  *logEvidence    = logZ;
  *varLogEvidence = varZ;

  if(data->verboseFlag)fclose(tfile);
  fprintf(stdout,"\n");

}

void PTMCMC(struct Model **model, struct Chain *chain, long *seed, int NC)
{
  /*
   NC is passed to PTMCMC redundendtly because the main code
   can reduce the number of chains based on the temperature
   of the hottest chain
   */
  int a, b;
  int olda, oldb;

  double heat1, heat2;
  double logL1, logL2;
  double dlogL;
  double H;
  double alpha;
  double beta;

  //b = (int)(ran2(seed)*((double)(chain->NC-1)));
  for(b=NC-1; b>0; b--)
  {
    a = b - 1;
    chain->A[a]=0;

    olda = chain->index[a];
    oldb = chain->index[b];

    heat1 = chain->temperature[a];
    heat2 = chain->temperature[b];

    logL1 = model[olda]->logL + model[olda]->logLnorm;
    logL2 = model[oldb]->logL + model[oldb]->logLnorm;

    //Hot chains jump more rarely
    if(ran2(seed)<1.0)///heat1)
    {
      dlogL = logL2 - logL1;
      H  = (heat2 - heat1)/(heat2*heat1);

      alpha = exp(dlogL*H);
      beta  = ran2(seed);

      chain->ptprop[a]++;

      if(alpha >= beta)
      {
        chain->ptacc[a]++;
        chain->index[a] = oldb;
        chain->index[b] = olda;
        chain->A[a]=1;
      }
    }
  }
}

void EvolveBayesLineParameters(struct Data *data, struct Model **model, struct BayesLineParams ***bayesline, struct Chain *chain, struct Prior *prior, int ic)
{
  int ifo,i;

  int N  = data->N;
  int NI = data->NI;

  struct Wavelet *wave;

  //pointers to structures
  struct Model *model_x = model[chain->index[ic]];
  struct BayesLineParams **bl_x = bayesline[chain->index[ic]];

  int priorFlag = 1;
  if(data->cleanFlag) priorFlag = 0;

  //update PSD for each interferometer
  for(ifo=0; ifo<NI; ifo++)
  {
    //copy over current multi-template & current residual
    for(i=0; i<N; i++)
    {
      data->r[ifo][i] = data->s[ifo][i];
      if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];
    }
    //re-run Markovian, full spectrum, full model part of BayesLine
    BayesLineRJMCMC(bl_x[ifo], data->r[ifo], model_x->Snf[ifo], model_x->invSnf[ifo], model_x->SnS[ifo], N, chain->cycle, chain->beta, priorFlag);
  }

  Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);

  //recompute likelihoods & priors of current chain
  model_x->logLnorm = 0.0;
  model_x->logL = 0.0;
  data->logLc = 0.0;

  for(ifo=0; ifo<NI; ifo++)
  {
    for(i=0; i< N; i++)
    {
      data->r[ifo][i] = data->s[ifo][i];
      if(data->signalFlag) data->r[ifo][i] -= model_x->response[ifo][i];
      if(data->glitchFlag) data->r[ifo][i] -= model_x->glitch[ifo]->templates[i];
    }

    model_x->detLogL[ifo] = loglike(data->imin, data->imax, data->r[ifo], model_x->Sn[ifo], model_x->invSnf[ifo]);
    if(!data->constantLogLFlag)
    {
      model_x->logL += model_x->detLogL[ifo];
      for(i=0; i<N/2; i++)
      {
        model_x->logLnorm -= log(model_x->Snf[ifo][i]);
      }
    }

    wave = model_x->glitch[ifo];
    wave->logp = 0.0;
    for(i=1; i<=wave->size; i++)
    {
      wave->logp += (data->glitch_amplitude_prior(wave->intParams[wave->index[i]],model_x->Snf[ifo], data->Tobs, prior->gSNRpeak));
    }
  }

  wave = model_x->signal;
  wave->logp = 0.0;
  for(i=1; i<=wave->size; i++)
  {
    wave->logp += (data->glitch_amplitude_prior(wave->intParams[wave->index[i]],model_x->SnGeo, data->Tobs, prior->sSNRpeak));
  }

}

void EvolveIntrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, struct TimeFrequencyMap *tf, long *seed, int ic)
{

  int mc;
  int i,j,k;
  int ii=0,jj,kk;
  int ifo;
  int rj;
  int typ;
  int det;
  int test;
  int acc;
  double alpha;
  double q,logqx,logqy;
  double logH;
  double Tobs = data->Tobs;

  int fisherFlag;
  int phaseFlag;
  int clusterFlag;
  int uniformFlag;
  int densityFlag;
    int NW = data->NW;

  //ratio of TF proposals to proximity proposals
  double TFtoProx = data->TFtoProx;
  double ClusterRate = 0.9;
  double FisherRate = 0.8;

    //optimize ratio for sampling prior
  if(data->constantLogLFlag || ic > 10)
  {
      FisherRate = 0.2;
      ClusterRate = 0.2;
  }

  //Unpack structures and use convenient (and familiar) names

  /* DATA */
  int N  = data->N;
  int NI = data->NI;

  /* PRIOR */
  int dmax = 1;

  double Snmin   = prior->Snmin;
  double Snmax   = prior->Snmax;
  double **range = prior->range;

  /* MODEL */
  struct Model *model_x = model[chain->index[ic]];

  model_x->size=model_x->signal->size;
  for(ifo=0; ifo<NI; ifo++) model_x->size+=model_x->glitch[ifo]->size;

  double *Snx = model_x->Sn;

  struct Wavelet *wave_x;
  int *larrayx;
  double **paramsx;

  /* CHAIN */
  int M = chain->cycle;//*(model_x->size+1);

  /* PROPOSED MODEL */
  struct Model *model_y = malloc(sizeof(struct Model));
  initialize_model(model_y, data, prior, model_x->Snf, seed);
  copy_ext_model(model_x, model_y, N, NI);
  copy_int_model(model_x, model_y, N, NI, data->NW, -1);
  copy_psd_model(model_x, model_y, N, NI);

  model_y->size=model_x->size;

  double *Sny = model_y->Sn;

  struct Wavelet *wave_y;
  int    *larrayy;
  double **paramsy;

  if(data->signalFlag) Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);

  acc=1;
  if(data->signalFlag) copy_int_model(model_x, model_y, N, NI, data->NW, -1);
  if(data->glitchFlag) for(ifo=0; ifo<NI; ifo++) copy_int_model(model_x, model_y, N, NI, data->NW, ifo);

  for(mc=1; mc<=M; mc++)
  {
    //Flags for which proposal is being used
    uniformFlag=0;
    clusterFlag=0;
    densityFlag=0;
    fisherFlag=0;
    phaseFlag=0;

    //acc gets set to 1 if model_y gets accepted (no need to copy at next iteration)
    if(acc==0) copy_int_model(model_x, model_y, N, data->NI, data->NW, det);
    if(data->psdFitFlag) for(ifo=0; ifo<NI; ifo++) Sny[ifo] = Snx[ifo];
    else                 for(ifo=0; ifo<NI; ifo++) Sny[ifo] = 1.0;

    //reset acceptence counter
    acc=0;

    //decide if we're doing a noise or signal update
    if(!data->signalFlag)
      alpha = 1.0;
    else if(data->signalFlag && !data->glitchFlag )
      alpha = 0.0;
    else
      alpha = ran2(seed);

    //update geocenter model
    if(alpha<chain->modelRate)
    {
      det = -1;

      dmax = prior->smax;

      wave_x = model_x->signal;
      wave_y = model_y->signal;

    }
    //update noise model
    else
    {
      det = (int)floor(ran2(seed)*(float)NI);

      dmax = prior->gmax[det];

      wave_x = model_x->glitch[det];
      wave_y = model_y->glitch[det];

    }

    larrayx = wave_x->index;
    larrayy = wave_y->index;

    paramsx = wave_x->intParams;
    paramsy = wave_y->intParams;


    //set prior and proposal terms to 0 (updated with += later on logp)
    wave_x->logp = 0.0;
    wave_y->logp = 0.0;
    logqx        = 0.0;
    logqy        = 0.0;

    rj   = 0;
    test = 0;


    /*******************************************************/
    /*                                                     */
    /*              TRANS DIMENSION RJMCMC MOVE            */
    /*                                                     */
    /*******************************************************/
    if(ran2(seed) < chain->rjmcmcRate)
    {
      rj = 1;

      if(ran2(seed) < 0.5)  // try and add a new wavelet
      {
        wave_y->size = wave_x->size+1;
        model_y->size = model_x->size+1;
        typ = 2;
      }
      else // try and remove wavelet
      {
        wave_y->size = wave_x->size-1;
        model_y->size = model_x->size-1;

        typ = 3;
      }

      if(wave_y->size < 0 || model_y->size < prior->smin || wave_y->size >= prior->smax) test = 1;

      if(test == 0)
      {
        // death move
        if(wave_y->size < wave_x->size)
        {
          i=(int)(ran2(seed)*(double)(wave_x->size))+1; // pick a term to try and kill
          k = 0;
          for(j=1; j<= wave_x->size; j++)
          {
            if(j != i)
            {
              k++;
              larrayy[k] = larrayx[j];
              jj = larrayx[j];
              for(kk=0; kk < NW; kk++) paramsy[jj][kk] = paramsx[jj][kk];
            }
            if(j == i) ii = larrayx[j];  // take note of who's demise is being proposed
          }

          //reshift the indexing array for occupied wavelets
          if(wave_y->size == 0) for(j=1; j<=dmax; j++) larrayy[j] = j;

          // TF density for proposal
          if(data->clusterProposalFlag)
          {
            q = 0.0;
            if(det==-1)q += ClusterRate*TFtoProx*TFprop_density(paramsx[ii], range, tf, data->NI);
            else       q += ClusterRate*TFtoProx*TFprop_density(paramsx[ii], range, tf, det);

            q += ClusterRate*(1.-TFtoProx)*wavelet_proximity_density(paramsx[ii][1], paramsx[ii][0], paramsy, larrayy, wave_y->size, prior);
            q += (1.0-ClusterRate)/prior->TFV;
            logqx += log(q);
            logqy +=  0.0;

          }
          // Uniform in TF proposal
          else
          {
            logqx += -prior->logTFV;
            logqy += 0.0;
          }

          if(data->amplitudePriorFlag)
          {
            if(det==-1) logqx += ( data->signal_amplitude_prior(paramsx[ii], model_x->SnGeo, data->Tobs, prior->sSNRpeak) );
            else        logqx += ( data->glitch_amplitude_prior(paramsx[ii], model_x->Snf[det], data->Tobs, prior->gSNRpeak) );
          }
        }//end death move

        // birth move
        if(wave_y->size > wave_x->size)
        {

          // find a label that isn't in use
          ii = 0;
          do
          {
            ii++;
            k = 0;
            for(j=1; j<= wave_x->size; j++)
            {
              if(ii==larrayx[j]) k = 1;
            }
          } while(k == 1);

          //reshift the indexing array for occupied wavelets
          larrayy[wave_y->size] = ii;

          // Draw all wavelet parameters from prior
          draw_wavelet_params(paramsy[ii], range, seed, data->NW);

          // TF density for proposal
          if(data->clusterProposalFlag )
          {
            if(ran2(seed)<ClusterRate)
            {

              // Draw new time and frequency from TFV density proposal
              if(det==-1) draw_time_frequency(data->NI, ii, wave_x, wave_y, range, seed, TFtoProx, tf, &k);
              else        draw_time_frequency(det, ii, wave_x, wave_y, range, seed, TFtoProx, tf, &k);
              if(ic==0)
              {
                if(k==0)
                {
                  densityFlag=1;
                  chain->dcount++;

                }
                if(k==1)
                {
                  clusterFlag=1;
                  chain->ccount++;
                }
              }
            }
            else
            {
              if(ic==0)
              {
                uniformFlag=1;
                chain->ucount++;
              }
            }

            q = 0.0;
            if(det==-1)q += ClusterRate*TFtoProx*TFprop_density(paramsy[ii], range, tf, data->NI);
            else       q += ClusterRate*TFtoProx*TFprop_density(paramsy[ii], range, tf, det);

            q += ClusterRate*(1.-TFtoProx)*wavelet_proximity_density(paramsy[ii][1], paramsy[ii][0], paramsx, larrayx, wave_x->size, prior);
            q += (1.0-ClusterRate)/prior->TFV;

            logqy += log(q);
            logqx +=  0.0;
          }
          // Uniform in TF proposal
          else
          {
            if(ic==0)
            {
              uniformFlag=1;
              chain->ucount++;
            }

            logqy += -prior->logTFV;
            logqx += 0.0;
          }

          if(data->amplitudeProposalFlag)
          {
            if(det==-1)
            {
              data->signal_amplitude_proposal(paramsy[ii], model_x->SnGeo, 1.0, seed, data->Tobs, prior->range, prior->sSNRpeak);
              logqy += ( data->signal_amplitude_prior(paramsy[ii],  model_x->SnGeo, data->Tobs, prior->sSNRpeak) );
            }
            else
            {
              data->glitch_amplitude_proposal(paramsy[ii], model_x->Snf[det], Sny[det], seed, data->Tobs, prior->range, prior->gSNRpeak);
              logqy += (data->glitch_amplitude_prior(paramsy[ii],  model_x->Snf[det], data->Tobs, prior->gSNRpeak) );
            }
          }
          else  logqy += -log(prior->range[3][1]-prior->range[3][0]);



        }//end birth move
      }//end test condition
    }//end trans-dimensional move

    /*******************************************************/
    /*                                                     */
    /*               FIXED DIMENSION MCMC MOVE             */
    /*                                                     */
    /*******************************************************/
    else
    {
      typ = 1;

      for(ifo=0; ifo<NI; ifo++)
      {
        if(data->psdFitFlag) Sny[ifo] = Snx[ifo]+gasdev2(seed)/sqrt(0.5*(double)N);
        if(Sny[ifo] > Snmax || Sny[ifo] < Snmin) test = 1;
      }

      //update stochastic background parameters
      if(data->stochasticFlag) stochastic_background_proposal(model_x->background, model_y->background, seed, &test);


      //Now update wavelet models if they are enabled
      if( (data->signalFlag || data->glitchFlag) && wave_x->size>0 )
      {
        i  = (int)(ran2(seed)*(double)(wave_x->size))+1; // pick a term to update
        ii = larrayx[i];

        alpha = ran2(seed);

        // replace existing wavelet (birth/death move)
        //TODO: Birth/Death move is currently disabled
        if(alpha > 1.0)//chain->intPropRate/chain->temperature[ic])
        {

          // Draw all wavelet parameters from prior
          draw_wavelet_params(paramsy[ii], range, seed, data->NW);

          // TF density for proposal
          if(data->clusterProposalFlag)
          {
            if(det==-1)
            {
              /* Draw new time and frequency from TFV density + proximity proposal */
              draw_time_frequency(data->NI, ii, wave_x, wave_y, range, seed, TFtoProx, tf, &k);
              logqx += log( TFtoProx*TFprop_density(paramsx[ii], range, tf, data->NI) + (1.-TFtoProx)*wavelet_proximity_density(paramsx[ii][1], paramsx[ii][0], paramsy, larrayy, wave_y->size, prior));
              logqy += log( TFtoProx*TFprop_density(paramsy[ii], range, tf, data->NI) + (1.-TFtoProx)*wavelet_proximity_density(paramsy[ii][1], paramsy[ii][0], paramsx, larrayx, wave_x->size, prior));
            }
            else
            {
              /* Draw new time and frequency from TFV density + proximity proposal */
              draw_time_frequency(det, ii, wave_x, wave_y, range, seed, TFtoProx, tf, &k);
              draw_time_frequency(data->NI, ii, wave_x, wave_y, range, seed, TFtoProx, tf, &k);
              logqx += log( TFtoProx*TFprop_density(paramsx[ii], range, tf, det) + (1.-TFtoProx)*wavelet_proximity_density(paramsx[ii][1], paramsx[ii][0], paramsy, larrayy, wave_y->size, prior));
              logqy += log( TFtoProx*TFprop_density(paramsy[ii], range, tf, det) + (1.-TFtoProx)*wavelet_proximity_density(paramsy[ii][1], paramsy[ii][0], paramsx, larrayx, wave_x->size, prior));
            }
          }

          // Uniform in TF proposal
          else
          {
            logqy += -prior->logTFV;
            logqx += -prior->logTFV;
          }

          if(data->amplitudeProposalFlag)
          {
            if(det==-1)
            {
              data->signal_amplitude_proposal(paramsy[ii], model_x->SnGeo, 1.0, seed, data->Tobs, prior->range, prior->sSNRpeak);
              logqy += (data->signal_amplitude_prior(paramsy[ii], model_x->SnGeo, data->Tobs, prior->sSNRpeak));
              logqx += (data->signal_amplitude_prior(paramsx[ii], model_x->SnGeo, data->Tobs, prior->sSNRpeak));

            }
            else
            {
              data->glitch_amplitude_proposal(paramsy[ii], model_x->Snf[det], Sny[det], seed, data->Tobs, prior->range, prior->gSNRpeak);
              logqy += (data->glitch_amplitude_prior(paramsy[ii], model_x->Snf[det], data->Tobs, prior->gSNRpeak));
              logqx += (data->glitch_amplitude_prior(paramsx[ii], model_x->Snf[det], data->Tobs, prior->gSNRpeak));
            }

          }
        }

        // perturb existing wavelet & PSD model
        else
        {
          /*
           proposal distribution uses the Fisher at x to propose y,
           then computes the Fisher at y to compute the proposal
           densities to maintain detailed balance
           */
          if(ran2(seed)<0.1)
          {
            if(ic==0)
            {
              phaseFlag=1;
              chain->pcount++;
            }

            intrinsic_halfcycle_proposal(paramsx[ii],paramsy[ii],seed);
          }
          else if(ran2(seed)<FisherRate)
          {
            if(ic==0)
            {
              fisherFlag=1;
              chain->fcount++;
            }
            //TODO: Try new fisher proposal

            if(det==-1) intrinsic_fisher_proposal(model_x, range, paramsx[ii], paramsy[ii], model_x->SnGeo,    1.0, &logqx, &logqy, seed, Tobs, &test, data->NW);
            else        intrinsic_fisher_proposal(model_x, range, paramsx[ii], paramsy[ii], model_x->Snf[det], 1.0, &logqx, &logqy, seed, Tobs, &test, data->NW);
            //if(det==-1) intrinsic_fisher_proposal_2(model_x,range, paramsx[ii], paramsy[ii], model_x->SnGeo,    1.0, &logqx, &logqy, seed, Tobs, &test);
            //else        intrinsic_fisher_proposal_2(model_x,range, paramsx[ii], paramsy[ii], model_x->Snf[det], 1.0, &logqx, &logqy, seed, Tobs, &test);
            
          }
          else
          {
            draw_wavelet_params(paramsy[ii], range, seed, data->NW);
            
            if(ran2(seed)<0.5 && data->amplitudeProposalFlag)
            {
              if(det==-1)
              {
                data->signal_amplitude_proposal(paramsy[ii], model_x->SnGeo, 1.0, seed, data->Tobs, prior->range, prior->sSNRpeak);
                logqy += ( data->signal_amplitude_prior(paramsy[ii],  model_x->SnGeo, data->Tobs, prior->sSNRpeak) );
              }
              else
              {
                data->glitch_amplitude_proposal(paramsy[ii], model_x->Snf[det], Sny[det], seed, data->Tobs, prior->range, prior->gSNRpeak);
                logqy += (data->glitch_amplitude_prior(paramsy[ii],  model_x->Snf[det], data->Tobs, prior->gSNRpeak) );
              }
            }
          }
        }//end perturb step
      }//end !noiseOnlyFlag check

    }//end model update


    // If new parameters are within prior compute priors & likelihoods
    //if(wave_y->size>0) for(i=1; i<=wave_y->size; i++) test += checkrange(paramsy[larrayy[i]],prior->range);

    if(test == 0)
    {
      // compute the likelihood using appropriate functionic
      if(ic<chain->NC/2 || data->constantLogLFlag)
        model_y->logL = data->intrinsic_likelihood(typ, ii, det, model_x, model_y, prior, chain, data);
      else
        model_y->logL = EvaluateMarkovianLogLikelihood(typ, ii, det, model_x, model_y, prior, chain, data);

      if( (data->signalFlag || data->glitchFlag) && model_y->logL > -1.0e60)
      {
        // clustering prior
        if(data->clusterPriorFlag)
        {
          wave_y->logp += wavelet_proximity_prior(wave_y, prior);
          wave_x->logp += wavelet_proximity_prior(wave_x, prior);
        }

        //only works on glitch parameters
        else if(data->backgroundPriorFlag && data->glitchFlag)
        {
          //printf("frequencies of glitch:  %g %g\n",paramsx[larrayx[ii]][1],paramsy[larrayy[ii]][1]);
          for(i=1; i<=wave_x->size; i++) wave_x->logp += glitch_background_prior(prior, paramsx[larrayx[i]]);
          for(i=1; i<=wave_y->size; i++) wave_y->logp += glitch_background_prior(prior, paramsy[larrayy[i]]);
        }

        // uniform prior on time-frequency volume
        else
        {
          wave_x->logp += -(double)(wave_x->size)*prior->logTFV;
          wave_y->logp += -(double)(wave_y->size)*prior->logTFV;
        }


        //amplitude prior
        if(data->amplitudePriorFlag)
        {
          for(i=1; i<=wave_y->size; i++)
          {
            if(det==-1)
              wave_y->logp += (data->signal_amplitude_prior(paramsy[larrayy[i]], model_x->SnGeo, data->Tobs, prior->sSNRpeak));
            else
              wave_y->logp += (data->glitch_amplitude_prior(paramsy[larrayy[i]],model_x->Snf[det], data->Tobs, prior->gSNRpeak));
          }
          for(i=1; i<=wave_x->size; i++)
          {
            if(det==-1)
              wave_x->logp += (data->signal_amplitude_prior(paramsx[larrayx[i]],model_x->SnGeo, data->Tobs, prior->sSNRpeak));
            else
              wave_x->logp += (data->glitch_amplitude_prior(paramsx[larrayx[i]],model_x->Snf[det], data->Tobs, prior->gSNRpeak));
          }
        }
        else
        {
          wave_y->logp  += -(double)(wave_y->size)*log(prior->range[3][1]-prior->range[3][0]);
          wave_x->logp  += -(double)(wave_x->size)*log(prior->range[3][1]-prior->range[3][0]);
        }

        //dimension prior
        if(data->waveletPriorFlag)
        {
          wave_x->logp += prior->Nwavelet[wave_x->size];
          wave_y->logp += prior->Nwavelet[wave_y->size];
        }

      }

      // and finally, the Hastings ratio
      logH = (model_y->logL - model_x->logL)*chain->beta + wave_y->logp - wave_x->logp - logqy + logqx;

        /*
       if(model_x->size != model_y->size)
       {
         printf("%i: %i->%i logLy=%g logLx=%g py=%g px=%g qy=%g qx=%g, dx=%i,dy=%i,num=%g, den=%g, logH=%g\n",typ,model_x->size,model_y->size,model_y->logL,model_x->logL,wave_y->logp,wave_x->logp,logqy,logqx,wave_x->size,wave_y->size,wave_y->logp+ logqx,- wave_x->logp - logqy, logH);
         printf("    %lg\n",prior->Nwavelet[wave_x->size]);
         printf("    %lg\n",prior->Nwavelet[wave_y->size]);
         printf("dif=%lg\n",prior->Nwavelet[wave_y->size]-prior->Nwavelet[wave_x->size]);
      }
         */

      //if(chain->index[ic]==0 && data->stochasticFlag)printf("logLy=%g logLx=%g Ay=%g Ax=%g detC=%g logH=%g\n",model_y->logL-data->logLc,model_x->logL-data->logLc,model_x->background->logamp,model_y->background->logamp, model_y->background->detCij[data->N/4],logH);

    }// end prior test condition
    else logH = -1.0e60; // Rejection sample at prior boundaries


    // Update proposal counters for acceptance ratios
    if(ic==0)
    {
      if(rj) chain->scount++;
      else
      {
        if(wave_x->size>0)chain->mcount++;
      }
    }

    /*******************************************************/
    /*                                                     */
    /*      Metropolis-Hastings acceptance/rejection       */
    /*                                                     */
    /*******************************************************/

    // Update current position (model_x) depending on acceptance/rejection

    alpha = log(ran2(seed));

    if(logH > alpha)
    {
      acc = 1;

      copy_int_model(model_y, model_x, N, data->NI, data->NW, det);

      if(data->stochasticFlag) copy_background(model_y->background, model_x->background, data->NI, data->N/2);

      if(ic==0)
      {
        if(rj) chain->sacc++;
        else
        {
          if(wave_x->size>0)chain->macc++;
        }

        if(fisherFlag) chain->facc++;
        if(phaseFlag)  chain->pacc++;
        if(clusterFlag)chain->cacc++;
        if(densityFlag)chain->dacc++;
        if(uniformFlag)chain->uacc++;
      }
      wave_x->logp = wave_y->logp;
    }

  }//end of MCMC loop over mc

  free_model(model_y, data, prior);
  free(model_y);

}

void EvolveExtrinsicParameters(struct Data *data, struct Prior *prior, struct Model **model, struct Chain *chain, long *seed, int ic)
{
  int i, j, ifo, mc;
  int dim;
  int test, iendN, ienddim;
  double logLx, logLy, logH;
  double alpha;
  double **glitch;
  double *geo;
  double *Sn;
  double *SnGeox;
  double *SnGeoy;
  double *paramsx, *paramsy;
  double **intParams;
  double logpy,logpx,logJ;

  //Unpack structures and use convenient (and familiar) names
  int NI = data->NI;
    int NW = data->NW;

  /* CHAIN */
  int M = 3*chain->cycle;

  /* MODEL */
  // extrinsic parameters
  struct Model *model_x = model[chain->index[ic]];
  struct Wavelet *smodel = model_x->signal;
  Sn      = model_x->Sn;
  geo     = smodel->templates;
  paramsx = model_x->extParams;
  SnGeox  = model_x->SnGeo;

  paramsy = dvector(0,NE);
  SnGeoy  = dvector(0,data->N/2);

  dim = smodel->size;
  ienddim = dim+1;
  iendN = data->N/2;
  intParams = dmatrix(1,dim,0,NW-1);


  //Set extrinsic parameter fisher matrix (numerical derivatives of response to geocenter waveform)
  extrinsic_fisher_update(data, model_x);

  glitch = malloc(NI*sizeof(double*));
  for(ifo=0; ifo<NI; ifo++) glitch[ifo] = model_x->glitch[ifo]->templates;

  //Initialize parameter vectors & likelihood for extrinsic parameter MCMC
  for(i=0; i<NE; i++)  paramsy[i] = paramsx[i];

  //TODO: Why can't I use a band-limited response for xtrinsic moves?
  //Related to computeProjectionCoeffs()?
  
  //Find minimum and maximum frequencies of signal model and only compute logL over that range
  /*
   double fmin = data->fmax;
  double fmax = data->fmin;
  double fi,ff;
  for(i=1; i<ienddim; i++)
  {
    model_x->wavelet_bandwidth(smodel->intParams[smodel->index[i]],&fi,&ff);
    if(fi<fmin) fmin=fi;
    if(ff>fmax) fmax=ff;

  }
    //Make sure frequency is in range
    if(fmin < data->fmin)
        fmin = data->fmin;
    if(fmax > data->fmax)
        fmax = data->fmax;
   */
    double fmin = data->fmin;
    double fmax = data->fmax;
    
  logLx = data->extrinsic_likelihood(model_x->projection, paramsx, model_x->invSnf, Sn, geo, glitch, data, fmin, fmax);

  //Compute Fisher Matrix for current sky location
  struct FisherMatrix *fisher = model_x->fisher;

  double draw;

  logpx=0.0;
  if(data->amplitudePriorFlag)
  {
    //model_x->projection has be updated by data->extrinsic_likelihood, so it stores F+ and Fx for params_y.
    Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);
    for(i=1; i<ienddim; i++)
    {
      logpx += (data->signal_amplitude_prior(smodel->intParams[smodel->index[i]],SnGeox, data->Tobs, prior->sSNRpeak));
    }
  }
  //logpx += dim*log(paramsx[5]);

  int sky=0;
  int cnt=0;
  int acc=0;
  for(mc=0; mc < M; mc++)
  {
    logpy=0.;
    logJ=0.;
    sky=0;
    test=0;
    for(i=1;i<ienddim;i++) for(j=0; j<NW; j++) intParams[i][j] = smodel->intParams[smodel->index[i]][j];
    for(i=0; i<iendN; i++) SnGeoy[i]=SnGeox[i];

    //Initialize parameter vectors & likelihood for extrinsic parameter MCMC
    for(i=0; i<NE; i++)  paramsy[i] = paramsx[i];

    /*
     Sky-ring proposal rotates zenith to 
     point along line connecting pair of detectors,
     holds declination fixed and draws azimuth from [0,2pi]
     
     Only works if we have more than one detector.
     
     Don't bother for hot chains, which should largely be
     sampling from the prior
     */
    if(ran2(seed)< 0.5 && chain->index[ic]<10 && NI>1)
    {
      if(ic==0)cnt++;
      if(ic==0)sky=1;
      sky_ring_proposal(paramsx,paramsy,data,seed);
      if(data->orientationProposalFlag && ran2(seed)<0.5)
        network_orientation_proposal(paramsx, paramsy, data, &logJ);
      else
        uniform_orientation_proposal(paramsy, seed);
    }
    else
    {
      /* Uniform Draw */
      draw = ran2(seed);
      if(draw<0.1)extrinsic_uniform_proposal(seed,paramsy);

      /* Fisher Matrix Proposal */
      else
      {
        fisher_matrix_proposal(fisher, paramsx, seed, paramsy);
      }
    }
    if(!data->extrinsicAmplitudeFlag) paramsy[5]=1.0;

    //Rescale intrinsic amplitudes and phases with extrinsic parameters
    for(i=1; i<ienddim; i++)
    {
      intParams[i][3] = smodel->intParams[smodel->index[i]][3]*paramsy[5];//amplitude
      test += checkrange(intParams[i],prior->range,NW);
    }

    if(test==0)
    {
      logLy = data->extrinsic_likelihood(model_x->projection, paramsy, model_x->invSnf, Sn, geo, glitch, data, fmin, fmax);

      //amplitude prior
      if(data->amplitudePriorFlag)
      {
        //model_x->projection has be updated by data->extrinsic_likelihood, so it stores F+ and Fx for params_y.
        if(data->geocenterPSDFlag)Shf_Geocenter(data, model_x, SnGeoy, paramsy);
        for(i=1; i<ienddim; i++) logpy += (data->signal_amplitude_prior(intParams[i],SnGeoy, data->Tobs, prior->sSNRpeak));
      }

      logH = (logLy - logLx)*chain->beta + logpy - logpx + logJ;
      
      alpha = log(ran2(seed));

      if(logH >= alpha)
      {
        chain->xacc++;
        logLx = logLy;
        for(i=0; i< NE; i++) paramsx[i] = paramsy[i];
        for(i=0; i<iendN; i++) SnGeox[i]=SnGeoy[i];
        logpx = logpy;

        if(sky==1 && ic==0) acc++;
      }
    }

    sky=0;

    chain->xcount++;

  }

  //Now update the full model with current extrinsic parameters
  for(i=0; i<data->N; i++) geo[i] *= model_x->extParams[5];

  //Rescale each wavelet amplitude
  for(i=1;i<ienddim;i++) model_x->signal->intParams[ model_x->signal->index[i]][3] *= model_x->extParams[5]; //amplitude

  //Reset amplitude scale factor to 1
  model_x->extParams[5] = 1.0;//amplitude


  computeProjectionCoeffs(data, model_x->projection, model_x->extParams, data->fmin, data->fmax);
  waveformProject(data, model_x->projection, model_x->extParams, model_x->response, model_x->signal->templates, data->fmin, data->fmax);
  Shf_Geocenter_full(data, model_x->projection, model_x->Snf, model_x->SnGeo, model_x->extParams);


  free(glitch);
  free_dvector(paramsy,0,NE);
  free_dvector(SnGeoy,0,iendN);
  free_dmatrix(intParams,1,dim,0,NW-1);
}

/* ********************************************************************************** */
/*                                                                                    */
/*                                    MCMC tools                                      */
/*                                                                                    */
/* ********************************************************************************** */


void adapt_temperature_ladder(struct Chain *chain, int NC)
{
  int ic;//,i,ifo;
  //  double x;

  double S[NC];
  double A[NC][2];

  double nu=10;
  double t0=10000;

  for(ic=1; ic<NC-1; ic++)
  {
    S[ic] = log(chain->temperature[ic] - chain->temperature[ic-1]);
    A[ic][0] = chain->A[ic-1];
    A[ic][1] = chain->A[ic];
  }

  ic=0;
  for(ic=1; ic<NC-1; ic++)
  {
    S[ic] += (A[ic][0] - A[ic][1])*(t0/((double)chain->mc+t0))/nu;
    //S[ic] += (A[ic][0] - A[ic][1])*(mc)/nu;
    chain->temperature[ic] = chain->temperature[ic-1] + exp(S[ic]);

  }//end loop over ic
}//end adapt function
