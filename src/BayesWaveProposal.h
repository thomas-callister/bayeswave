#ifdef __GNUC__
#define UNUSED __attribute__ ((unused))
#else
#define UNUSED
#endif



/* ********************************************************************************** */
/*                                                                                    */
/*                                    Prior proposals                                 */
/*                                                                                    */
/* ********************************************************************************** */

void extrinsic_uniform_proposal(long *seed, double *y);

void draw_glitch_amplitude(double *params, double *Snf, double Sa, long *seed, double Tobs, double **range, double SNRpeak);

void draw_signal_amplitude(double *params, double *Snf, double Sa, long *seed, double Tobs, double **range, double SNRpeak);

void intrinsic_halfcycle_proposal(double *x, double *y, long *seed);

/* ********************************************************************************** */
/*                                                                                    */
/*                                Fisher matrix proposals                             */
/*                                                                                    */
/* ********************************************************************************** */

void fisher_matrix_proposal(struct FisherMatrix *fisher, double *params, long *seed, double *y);

void extrinsic_fisher_information_matrix(struct FisherMatrix *fisher, double *params, double **invSnf, double *Sn, double *geo, double **glitch, struct Data *data);

void extrinsic_fisher_update(struct Data *data, struct Model *model);

void intrinsic_fisher_update(double *params, double *dparams, double *Snf, double Snx, double Tobs, int NW);

void intrinsic_fisher_proposal(UNUSED struct Model *model, double **range, double *paramsx, double *paramsy, double *Snf, double Snx, double *logpx, double *logpy, long *seed, double Tobs, int *test, int NW);

void intrinsic_fisher_proposal_2(struct Model *model, double **range, double *paramsx, double *paramsy, double *Snf, double Snx, double *logpx, double *logpy, long *seed, double Tobs, int *test, int NW);

/* ********************************************************************************** */
/*                                                                                    */
/*                       Custom intrinsic proposal distributions                      */
/*                                                                                    */
/* ********************************************************************************** */

double wavelet_proximity_density(double f, double t, double **params, int *larray, int cnt, struct Prior *prior);

void wavelet_proximity(double *paramsy, double **params, int *larray, double **range, int cnt, long *seed);

void wavelet_proximity_new(double *paramsy, double **params, int *larray, double **range, int cnt, long *seed);

void wavelet_sample(double *params, double **range, double **prop, int tsize, double Tobs, double pmax, long *seed);

void draw_wavelet_params(double *params, double **range, long *seed, int NW);

void draw_time_frequency(int ifo, int ii, struct Wavelet *wx, struct Wavelet *wy, double **range, long *seed, double fr, struct TimeFrequencyMap *tf, int *prop);

void TFprop_draw(double *params, double **range, struct TimeFrequencyMap *tf, int ifo, long *seed);

double TFprop_density(double *params, double **range, struct TimeFrequencyMap *tf, int ifo);

void TFprop_setup(struct Data *data, struct Model *model, double **range, struct TimeFrequencyMap *tf, int ifo);

void TFprop_plot(double **range, struct TimeFrequencyMap *tf, double dt, int ifo);

void TFprop_signal(struct Data *data, double **range, struct TimeFrequencyMap *tf, struct Network *net);

/* ********************************************************************************** */
/*                                                                                    */
/*                       Custom extrinsic proposal distributions                      */
/*                                                                                    */
/* ********************************************************************************** */

void sky_ring_proposal(double *x, double *y, struct Data *data, long *seed);

void uniform_orientation_proposal(double *y, long *seed);

void network_orientation_proposal(double *paramsx, double *paramsy, struct Data *data, double *logJ);


/* ********************************************************************************** */
/*                                                                                    */
/*                          Stochastic background proposals                           */
/*                                                                                    */
/* ********************************************************************************** */

void stochastic_background_proposal(struct Background *bkg_x, struct Background *bkg_y, long *seed, int *test);
